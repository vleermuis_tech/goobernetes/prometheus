# note: this is now being maintained at https://github.com/small-hack/argocd/tree/main/kube-prometheus-stack

# Repo for deploying Prometheus stack to k8s with ArgoCD

This will deploy:

- Prometheus
- Alert Manager for prometheus
- Grafana

Ref for prometheus giant chart of charts:
https://blog.ediri.io/kube-prometheus-stack-and-argocd-23-how-to-remove-a-workaround
